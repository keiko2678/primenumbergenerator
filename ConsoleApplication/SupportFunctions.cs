﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSharp.Framework;

namespace ConsoleApplication
{
	static class SupportFunctions
	{
		public static int GetMax(int numberOfDigits)
		{
			int num = 9;
			for (int i = 1; i< numberOfDigits; i++)
			{
				num *= 10;
				num +=9;
			}
			return num;
		}

		public static int ReadNumber(string t, int min, int max)
		{
			int tall = 0;                         
			do
			{
				Console.Write( "\t" + t  + " (" + min + '-' + max + "):  ");
				tall =Console.ReadLine().To<int>();

			} while (tall < min || tall > max);   
			return tall;
		}

		
		public static int countLength(this int num)
		{
			int count = 0;                          
			while (num >= 1)
			{                     
				num /= 10;
				count++;
			}
			return count;
		}


		public static char Read(string t)
		{
			char ch;                              // Hjelpechar
			Console.Write(t);
			ch = Console.ReadKey().KeyChar;
			Console.ReadLine();
			Console.Write("\n\n");
			return (ch.ToUpper());                 // Send tilbake toupper
		}

		public static void DefineNumberOfDigits(ref int min, ref int max, int MAXSIFFER)
		{
			int siffer = ReadNumber("Write number of digits", 1, MAXSIFFER);
			min = max = 0;
			for (int i = 1; i < siffer; i++)
			{
				if (min == 0)
					min = 10;
				else
					min *= 10;
				max += min * 9;
			}
			max += 9;
		}

		/* Lager random nr på gitt antall siffer
		 * Returnerer et random generert nummer */
		public static int GenerateRandom(int digits)
		{
			
			var random = new Random(LocalTime.Now.Millisecond);

			int num = (random.Next() % 9) + 1;
			for (int i = 1; i < digits; i++)
			{                   
					num *= 10;                                        
					num += Math.Abs( random.Next() % 10);                               
			}
			return num;
		}

	}
}

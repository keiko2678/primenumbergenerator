﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
	static class ArrayBased
	{
		public static int eXtremeRandomPrime(int digits, int arrayLength)
		{
			var randomNumber = SupportFunctions.GenerateRandom(digits);

			var numbers = GeneratePrimesParalell(randomNumber, randomNumber + arrayLength, true);

			if (numbers.Any())
			{
				Console.WriteLine("\tRandom primenumber: "+ String.Format("{0:n0}", numbers.First()));
				return numbers.First();
			}
			else
				return 0;
		}


		
		public static IEnumerable<int> GeneratePrimesSerial(int lower, int higher, bool verbose = true)
		{
			var result = new List<int>();
			bool [] numbers = new bool[higher - lower];
			var startTime = LocalTime.Now;
			int temp;

			Console.WriteLine("\tStarting array length is " + string.Format("{0:n0}", numbers.Length) 
				+ " - lower value is " + string.Format("{0:n0}", lower)
				+ " - total digits " + string.Format("{0:n0}",higher.countLength()));

			if (verbose) Console.Write("\n\tPreparing - ");

			for (int i = 0; i < (higher - lower); i++)
				numbers[i] = true;

			if (verbose) Console.Write("Generating - ");                                
			for (int i = 2; i <= higher / 2; i++)
			{																				
				int j = lower / i;                                       
				while (j >= lower)
					j--;

				if (j < 2)                                                
					j = 2;

				for (; j <= higher / i; j++)
				{																			
					temp = j * i;
					if (temp >= lower && temp < higher)
						if (numbers[temp - lower])
							numbers[temp - lower] = false;
				}
			}

			if (verbose) Console.Write("Preparing result - ");

			for (int i = 0; i < (higher - lower); i++)
				if (numbers[i])
					result.Add(i + lower);


			if (verbose) Console.Write("Generated");
			var endTime = LocalTime.Now;

			
				Console.WriteLine("\n\tThe operation used " + endTime.Subtract(startTime).TotalSeconds.ToString("0.##") + " seconds to finish.");

			if (verbose) Console.WriteLine("\n\t" + String.Format("{0:n0}",result.Count) + " number" + (result.HasMany() ? "s" : "") + " was found within the search");

			return result;
		}


		public static IEnumerable<int> GeneratePrimesParalell(int lower, int higher, bool verbose = true)
		{
			var result = new List<int>();
			bool[] numbers = new bool[higher - lower];
			var startTime = LocalTime.Now;

			Console.WriteLine("\tStarting array length is " + string.Format("{0:n0}", numbers.Length)
				+ " - lower value is " + string.Format("{0:n0}", lower)
				+ " - total digits " + string.Format("{0:n0}", higher.countLength()));

			if (verbose)
				Console.Write("\n\tPreparing - ");

			for (int i = 0; i < (higher - lower); i++)
				numbers[i] = true;

			if (verbose)
				Console.Write("Generating - ");


			Parallel.For(2, (higher / 2) + 1, i=> CheckNumbers(lower, higher, i, ref numbers));
			

			if (verbose)
				Console.Write("Preparing result - ");

			for (int i = 0; i < (higher - lower); i++)
				if (numbers[i])
					result.Add(i + lower);


			if (verbose)
				Console.Write("Generated");
			var endTime = LocalTime.Now;


			Console.WriteLine("\n\tThe operation used " + endTime.Subtract(startTime).TotalSeconds.ToString("0.##") + " seconds to finish.");

			if (verbose)
				Console.WriteLine("\n\t" + String.Format("{0:n0}", result.Count) + " number" + (result.HasMany() ? "s" : "") + " was found within the search");

			return result;
		}

		public static void CheckNumbers(int lower, int higher, int i, ref bool[] numbers )
		{
			int temp;
			int j = lower / i;
			while (j >= lower)
				j--;

			if (j < 2)
				j = 2;

			for (; j <= higher / i; j++)
			{
				temp = j * i;
				if (temp >= lower && temp < higher)
					if (numbers[temp - lower])
						numbers[temp - lower] = false;
			}
		}
	}
}

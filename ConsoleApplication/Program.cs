﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
	class Program
	{

		const int MAXDIGITS = 9;
		const int eXtremeINTERVAL = 50;

		static void Main(string[] args)
		{
			showCredits();
			RunMenu();  
		}

		public static void RunMenu()
		{
			char kommando;
			int lower = 0;
			int higher = SupportFunctions.GetMax(8);

			ShowMenu(lower, higher);
			kommando = SupportFunctions.Read("\n\tCommand:  ");      
			while (kommando != 'Q')
			{
				switch (kommando)
				{
					case 'G': 
						ArrayBased.GeneratePrimesParalell(lower, higher - lower);
						break;
					case 'E':                                           
						lower = SupportFunctions.ReadNumber("Write lower value", 1, SupportFunctions.GetMax(MAXDIGITS));     
						higher= SupportFunctions.ReadNumber("Write higher value", lower, SupportFunctions.GetMax(MAXDIGITS));   
						break;
					case 'S':                                             // Endre verdier - ant siffer
						SupportFunctions.DefineNumberOfDigits(ref lower, ref higher, MAXDIGITS);
						break;
					case 'R':                                           // Henter random primtall fra range
						{
							Console.Write("\t\nRandom number: " + SupportFunctions.GenerateRandom(MAXDIGITS));
						}
						break;
					case 'X':                                           // Henter random primtall fra range
						{
							ArrayBased.eXtremeRandomPrime(higher.countLength(), eXtremeINTERVAL);
						}
						break;
					//	case 'U':	lagreRange();				skrivMeny();	break;        // Lagrer range til fil
					default:                                                  // Skriv meny
						ShowMenu(lower, higher);
						break;
				}
				ShowMenu(lower, higher);
				kommando = SupportFunctions.Read("\n\tCommand:  ");                      //  Leser brukerens valg
			}
		}


		public static void showCredits()
		{
			Console.Write(	  "\n\t***********************************************"
								+ "\n\t*                                             *"
								+ "\n\t*  - - - - PRIME NUMBER GENERATOR - - - -     *"
								+ "\n\t*                                             *"
								+ "\n\t*  Version: 0.1                               *"
								+ "\n\t*  Made by: Per Christian Kofstad             *"
								+ "\n\t*                                             *"
								+ "\n\t*                                             *"
								+ "\n\t*                                             *"
								+ "\n\t***********************************************");
		}

		public static void ShowMenu(int lower, int higher)
		{           
			Console.Write("\n\n\n\tRange defined as " + string.Format("{0:n0}", lower) + " - " + string.Format("{0:n0}", higher));

			Console.Write("\n\n\tG - Generate new values"
					+ "\n\tX - Find Singel Random Number of x amount of digits"
					+ "\n\n\tE - Manually put range"
					+ "\n\tS - Define range by number of digits"
					+ "\n\n\tR - Test Random number in range"
					
					+ "\n\n\tQ - Avslutt program \n");
		}
	}
}
